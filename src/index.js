import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';



import {createStore, applyMiddleware, compose, combineReducers} from 'redux'
import thunkMiddleware from 'redux-thunk'

import {Provider} from 'react-redux'

import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';


import menuReducrs from './store/reducers/menuReducrs'

import cardReducer from './store/reducers/cardReducers'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;




const reducerRoot = combineReducers({
    menu: menuReducrs,
    card: cardReducer

});


const store = createStore(
    reducerRoot,
    composeEnhancers(applyMiddleware(thunkMiddleware))
)

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
);


ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

import axios from '../../axios'

import {ADD_ORDER_CARD, ORDER_SUCCESS, } from "./actionsType";

export  const orederMenuSuccess = (dishes) => ({type: ORDER_SUCCESS, dishes});

export const getMenu = () => {
    return dispatch => {
       axios.get('products.json').then(response => {
           dispatch(orederMenuSuccess(response.data))
       })
    }
};

export const  addOrderCard = (order) => ({type: ADD_ORDER_CARD,  order});



export const addCard = (order) => {
    return dispatch => {
        dispatch(addOrderCard(order))
    }
};

export const createOrder = (order) => {
    return dispatch => {
        axios.post('/Ordes.json', order).then(response => {
            console.log(response);
        })
    }

};
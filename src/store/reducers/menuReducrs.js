import {ORDER_SUCCESS} from "../actions/actionsType";

const initialState = {
    menu: []
};

const basketReducerOrdeer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_SUCCESS:
            return {
                ...state,
                menu: action.dishes
            };
        default:
            return state
    }

};


export default basketReducerOrdeer
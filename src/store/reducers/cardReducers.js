import {ADD_ORDER_CARD} from "../actions/actionsType";

const initialState = {
    basket: [],
    totalPrice: 0,
    Dapibus: 150
};

const cardReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_ORDER_CARD:
            let order;
            if (state.basket[action.order.name]) {
                order = {...action.order, count: state.basket[action.order.name].count + 1}
            } else {
                order = {...action.order, count: 1}
            }

            const orders = {...state.basket, [action.order.name]: order};

            return {
                ...state,
                basket: orders,
                totalPrice: state.totalPrice + order.price,
            };
        default:
            return state
    }

};


export default cardReducer
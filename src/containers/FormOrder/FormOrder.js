import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import { createOrder} from "../../store/actions/Menu";
import {connect} from "react-redux";

class FormOrder extends Component {
    state = {
        name: '',
        email: '',
        surname: '',
    };


    valueChange = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value})
    };

    orderHandler = (event) => {

        const orderData = {
            customer: {...this.state},
            orders: this.props.basket

        };

        this.props.createOreder(orderData)

    };

    render() {
        return (
            <Form onSubmit={this.orderHandler()}>
                <FormGroup row>
                    <Label  sm={2}>Name</Label>
                    <Col sm={10}>
                        <Input value={this.state.name} onChange={this.valueChange} type="text" name="name"  />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input value={this.state.email} onChange={this.valueChange} type="email" name="email"  />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label  sm={2}>Surname</Label>
                    <Col sm={10}>
                        <Input value={this.state.surname} onChange={this.valueChange} type="text" name="surname"/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button>clear order</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state =>({
    basket: state.card.basket,

});

const mapDispachToProps = dispatch => ({
    createOreder:  (order) => dispatch(createOrder(order))
});

export default connect(mapStateToProps,mapDispachToProps )(FormOrder);
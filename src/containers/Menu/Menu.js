import React, {Component} from 'react';
import {Button, Card, CardBody, CardDeck, CardImg, CardSubtitle, CardTitle, Col} from "reactstrap";
import Row from "reactstrap/es/Row";
import {connect} from "react-redux";
import {addCard, getMenu} from "../../store/actions/Menu";
import Basket from "../../component/Basket/Basket";
import Modal from "../../component/UI/Modal";
import FormOrder from "../FormOrder/FormOrder";

class Menu extends Component {

    state ={
      show: false
    };

 componentDidMount() {
     this.props.getMenu()
 }

 showModal = () => {
     this.setState({show: true})
 };

    render() {
        return (
                <Col>
                    <Modal show={this.state.show}>
                        <FormOrder/>
                    </Modal>
                    <Row>
                        <Col sm={5}>
                            <CardDeck>
                                {Object.keys(this.props.menu).map(dishes => {
                                    return (
                                    <Card>
                                        <CardImg top width="100%" src={this.props.menu[dishes].image} />
                                        <CardBody>
                                            <CardTitle>{this.props.menu[dishes].name}</CardTitle>
                                            <CardSubtitle>{this.props.menu[dishes].cost}</CardSubtitle>
                                            <Button onClick={() => this.props.addCard({name: this.props.menu[dishes].name, price: this.props.menu[dishes].cost})}>Add card</Button>
                                        </CardBody>
                                    </Card>
                                    )
                                })}
                            </CardDeck>
                        </Col>
                        <Basket show={this.showModal} totalPrice={this.props.totalPrice} order={this.props.basket}/>
                    </Row>
                </Col>
        );
    }
}


const mapStateToProps = state =>({
    menu: state.menu.menu,
    basket: state.card.basket,
    totalPrice: state.card.totalPrice

});

const mapDispachToProps = dispatch => ({
     getMenu: () => dispatch(getMenu()),
     addCard: (order) => dispatch(addCard(order))
});


export default connect(mapStateToProps, mapDispachToProps)(Menu);
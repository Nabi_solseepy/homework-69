import React from 'react';
import {Col, ListGroup, ListGroupItem, Table} from "reactstrap";
import Button from "reactstrap/es/Button";

const Basket = (props) => {
        return (
                <Col sm={4}>
                <Table dark>
                    <tbody>
                    {Object.keys(props.order).map((dishes, id) => {
                        return (
                            <tr key={id}>
                                <td>{props.order[dishes].name}</td>
                                <td>x {props.order[dishes].count}</td>
                                <td>{props.order[dishes].price} KGS</td>
                            </tr>
                        )
                    })}

                    </tbody>
                </Table>
                    <ListGroup>
                        <ListGroupItem>total price {props.totalPrice}</ListGroupItem>
                        <ListGroupItem> Dapibus ac facilisis in{props.Dapibus}</ListGroupItem>
                        <Button onClick={props.show}>Place order</Button>
                    </ListGroup>
                </Col>
        );
};

export default Basket;